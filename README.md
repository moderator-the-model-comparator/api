# ModeRator API

This is API service for _Moderator - The Model Comparator_. Please see UI repository as well at https://gitlab.com/moderator-the-model-comparator/ui

### Local development
#### Setting up and running for the first time
```bash
# Create virtualenv
python3 -m venv venv

# Activate virtualenv
source venv/bin/activate

# install dependencies in the virtual environment
pip install -r requirements.txt

# Run Moderator API. You can access it on http://localhost:8000
uvicorn main:app --reload
```

#### Everyday development
```bash
source venv/bin/activate
uvicorn main:app --reload
```


### Running in Production
```bash
uvicorn main:app
# or if on Digitalocean Apps platform
gunicorn --worker-tmp-dir /dev/shm -b 0.0.0.0:8000 main:app -w 4 -k uvicorn.workers.UvicornWorker
```

