import uuid
import datetime
import pickle

from typing import Optional, Union, List

from fastapi import BackgroundTasks, FastAPI, HTTPException

import base64

import time

from pydantic import BaseModel, ValidationError, validator

import aioredis

from decouple import config, Csv

from fastapi.middleware.cors import CORSMiddleware

from libsbml import SBMLReader, readSBMLFromString


from stmodel import xml_sbml_model

# libsbml.readSBMLFromString()

# reader = SBMLReader()
# document = reader.readSBML("examples/sample-models/from-spec/level-2/enzymekinetics.xml")

# print(f"SBML errors: {document.getNumErrors()}")


if config("LOCAL_DEVELOPMENT", default=False, cast=bool):
    app = FastAPI(title="Localhost ModeRator - the Model Comparator")
    print("LOCAL_DEVELOPMENT")
else:
    app = FastAPI(title="ModeRator3 - the Model Comparator", root_path="/api")

app.add_middleware(
    CORSMiddleware,
    allow_origins=config("ALLOW_ORIGINS", cast=Csv()),
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

redis = aioredis.from_url(
    config("DATABASE_URL")
    # decode_responses=True,
)


class ComparisonSettings(BaseModel):
    phonetic: bool
    tolerate_h: int
    ignore_compartments: bool
    check_id: bool
    alpha: float
    beta: float
    low_limit: int
    left_limit: int
    right_limit: int
    left_or_right: bool
    only_missing: bool
    greedy_matching: bool
    filter_by_ec: bool
    all_reversible: bool
    reactants_threshold: int

    @validator("tolerate_h", "low_limit", "left_limit", "right_limit")
    def must_be_1_to_6(cls, v):
        if v > 6:
            raise ValueError("Too big")
        if v < 0:
            raise ValueError("Too small")
        return v

    @validator("reactants_threshold")
    def must_be_1_to_99(cls, v):
        if v > 99:
            raise ValueError("Too big")
        if v < 0:
            raise ValueError("Too small")
        return v


class ModelPack(BaseModel):
    model1: str
    model2: str
    settings: ComparisonSettings

    @validator("model1", "model2")
    def must_be_valid_sbml(cls, v):
        xml = base64.b64decode(v).decode("utf-8")
        doc = readSBMLFromString(xml)
        if doc.getNumErrors() > 0:
            raise ValueError(doc.getError(0).getShortMessage())
        return doc.getModel()


@app.get("/")
def read_root():
    return {"description": "This is ModeRator API. Please check /docs."}


@app.post("/upload")
def process_uploaded(modelpack: ModelPack):
    model_a = xml_sbml_model(modelpack.model1)
    model_b = xml_sbml_model(modelpack.model2)

    # for x in model_a.compartments:
    #     print(x)

    # print("-----")
    # for x in model_b.compartments:
    #     print(type(x))

    # model_a, model_b = on_apply_compartment_mapping(model_a, model_b)

    # print("====================================")
    # for x in model_a.compartments:
    #     print(x)

    # print("-----")
    # for x in model_b.compartments:
    #     print(x)

    # model_a.link_metabolites_with_other(model_b)

    return {
        "summary": {
            "compatments_a": len(model_a.compartments),
            "metabolites_a": len(model_a.metabolites),
            "reactions_a": len(model_a.reactions),
            "compatments_b": len(model_b.compartments),
            "metabolites_b": len(model_b.metabolites),
            "reactions_b": len(model_b.reactions),
        }
    }


# get rid of the rest (at some point)


# @app.get("/items/{item_id}")
# def read_item(item_id: int, q: Optional[str] = None):
#     return {"item_id": item_id, "q": q}


# def long_process(iterations: int, delay: int):
#     start_time = time.time()
#     for i in range(iterations):
#         time.sleep(delay)
#         print(f"{i}/{iterations}")
#     print(f"Task took {round(time.time() - start_time)} seconds to run.")


# @app.get("/mock-comparison/{iterations}/{delay}")
# async def start_comparison(
#     iterations: int, delay: int, background_tasks: BackgroundTasks
# ):
#     background_tasks.add_task(long_process, iterations, delay)
#     return {"message": "Comparison started in the background"}
